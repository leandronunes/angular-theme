import { Component, Inject, EventEmitter, Input } from "ng-forward";
import { LanguageSelectorComponent } from "../language-selector/language-selector.component";
import { SessionService, AuthService, AuthController, AuthEvents } from "./../../login";
import { EnvironmentService } from "./../../../lib/ng-noosfero-api/http/environment.service";
import { SidebarNotificationService } from "../sidebar/sidebar.notification.service";
import { BodyStateClassesService } from '../services/body-state-classes.service';
import { DesignModeTogglerComponent } from './../../admin/layout-edit/designModeToggler.component';
import { BootstrapSwitcherComponent, BootstrapSwitcherItem } from './../../shared/components/bootstrap-switcher/bootstrap-switcher.component';

@Component({
    selector: "acme-navbar",
    templateUrl: "app/layout/navbar/navbar.html",
    directives: [LanguageSelectorComponent, DesignModeTogglerComponent, BootstrapSwitcherComponent],
    providers: [AuthService, SessionService, SidebarNotificationService, EnvironmentService]
})
@Inject("$uibModal", AuthService, "SessionService", "$state", SidebarNotificationService, BodyStateClassesService, EnvironmentService)
export class Navbar {

    private currentUser: noosfero.User;
    private modalInstance: ng.ui.bootstrap.IModalServiceInstance;
    public showHamburger: boolean = false;
    public currentEnvironment: noosfero.Environment = <any>{ name: '' };
    /**
     *
     */
    constructor(
        private $uibModal: ng.ui.bootstrap.IModalService,
        public authService: AuthService,
        private session: SessionService,
        private $state: ng.ui.IStateService,
        private sidebarNotificationService: SidebarNotificationService,
        private bodyStateService: BodyStateClassesService,
        private environmentService: EnvironmentService
    ) {
        this.currentUser = this.session.currentUser();
        this.currentEnvironment = environmentService.getCurrentEnvironment();

        this.showHamburger = this.authService.isAuthenticated();
        this.bodyStateService.addContentClass(!this.sidebarNotificationService.sidebarVisible);

        this.authService.subscribe(AuthEvents[AuthEvents.loginSuccess], () => {
            if (this.modalInstance) {
                this.modalInstance.close();
                this.modalInstance = null;
            }

            this.currentUser = this.session.currentUser();
            this.showHamburger = true;

            this.$state.go(this.$state.current, {}, { reload: true });  // TODO move to auth
        });

        this.authService.subscribe(AuthEvents[AuthEvents.logoutSuccess], () => {
            this.currentUser = this.session.currentUser();
        });

    }

    public toggleCollapse() {
        this.sidebarNotificationService.alternateVisibility();

        this.bodyStateService.addContentClass(!this.sidebarNotificationService.sidebarVisible);
    }

    openLogin() {
        this.modalInstance = this.$uibModal.open({
            templateUrl: 'app/login/login.html',
            controller: AuthController,
            controllerAs: 'vm',
            bindToController: true
        });
    };

    logout() {
        this.authService.logout();
        this.$state.go(this.$state.current, {}, { reload: true });  // TODO move to auth
    };

    activate() {
        if (!this.currentUser) {
            this.openLogin();
        }
    }

}
