namespace noosfero {
    /**
     * @ngdoc interface
     * @name noosfero.Community
     * @description
     *  A representation of a Community in Noosfero.
     */
    export interface Community extends Profile {

    }
}
