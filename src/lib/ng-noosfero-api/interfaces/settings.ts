namespace noosfero {
    /**
     * Represents a noosfero block settings.
     */
    export interface Settings {
        sections: noosfero.Section[];
        limit: number;
    }
}